import React from 'react';
import Header from '../components/Header';
import ArticleList from '../components/ArticleList';
import MostRead from '../components/MostRead';
import Charts from '../components/Charts';

export default class Main extends React.Component {
	
	render() {				
		return (
			<div>
				<div className="container">
					<Header />
					<div className="row">
						<div className="col-md-offset-1 col-md-6">
							<ArticleList 
								dismissArticle={this.props.dismissArticle}
								articles={this.props.articles} />
						</div>
						<div className="col-md-offset-1 col-md-4">
							<Charts fetchChart={this.props.fetchChart} chartData={this.props.chartData}/>
							<MostRead />							
						</div>
					</div>
				</div>
			</div>			
		);
	}
}