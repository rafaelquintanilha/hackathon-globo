import React, { PropTypes } from 'react';
import moment from "moment";
import { CSS_COLORS } from '../constants/css';
import SocialBar from '../components/SocialBar';

export default class Article extends React.Component {

	constructor(props) {
		super(props);
		this.state = {			
			borderStyle: "solid",
			borderColor: "white"
		};
	}

	formatDate(date) {
		const diff = moment.duration(moment(new Date()).diff(this.props.date));
		const hours = parseInt(diff.asHours());
		if ( hours > 23 ) {
			const days = parseInt(diff.asDays());
			const tail = days === 1 ? "dia" : "dias";
			return `há ${days} ${tail}`;
		}
		else if ( hours > 0 ) {
			const tail = hours === 1 ? "hora" : "horas";
			return `há ${hours} ${tail}`;
		}
		const minutes = parseInt(diff.asMinutes());
		const tail = minutes > 1 ? "minutos" : "minuto";
		return `há ${minutes} ${tail}`;
	} 

	onMouseOver() {		
		const color = CSS_COLORS[this.props.theme.toLowerCase()];
		this.setState({
			borderStyle: "solid",
			borderColor: color
		});
	}

	onMouseOut() {
		this.setState({
			borderStyle: "solid",
			borderColor: "white"
		});
	}
	
	render() {		
		const mediaHeadingClass = "media-heading " + this.props.theme.toLowerCase();
		const dateMessage = this.formatDate(this.props.date);
		// image is 16:9
		return (			
			<li className="media">					
				<div className="media-left">
					<a href="#">
						<div style={{paddingRight: "0px", ...this.state}}>
							<img
								onMouseOver={this.onMouseOver.bind(this)} 
								onMouseOut={this.onMouseOut.bind(this)} 
								className="media-object" 
								src={this.props.image} 
								style={{width: "200px", height: "auto"}}/>
						</div>
					</a>
				</div>
				<div className="media-body" style={{paddingLeft: "20px"}}>
					<p>
						<b>{this.props.tag}</b>
						<span className="pull-right">
							<i className="fa fa-clock-o"></i>
							&nbsp;
							<i>{dateMessage}</i>
						</span>
					</p>						
					<h3 className={mediaHeadingClass} style={{cursor: "pointer"}}>{this.props.title}</h3>
					<p style={{opacity: 0.8}}>{this.props.description}</p>
					<SocialBar dismissArticle={this.props.dismissArticle} index={this.props.index}/>			
				</div>					
			</li>
		);
	}
}

Article.propTypes = {
	theme: PropTypes.string.isRequired
};