import React from 'react';
import Bar from '../components/Bar';
//import Line from '../components/Line';

export default class Charts extends React.Component {

	componentWillMount() {
		this.props.fetchChart();
	}
	
	render() {		
		return (
			<div className="panel panel-info">
				<div className="panel-heading">
					<h3 className="panel-title"><b>Gráficos</b></h3>
				</div>
				<div className="panel-body">
					<Bar fetchChart={this.props.fetchChart} chartData={this.props.chartData} />
					<hr />					
				</div>
			</div>
		);
	}
}