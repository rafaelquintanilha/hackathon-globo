import React from 'react';
import MostReadRow from '../components/MostReadRow';

export default class MostRead extends React.Component {
	
	render() {

		const topNews = [
			{ text: "Rafael foi para o BBB", theme: "g1" },
			{ text: "Vasco é campeão mundial", theme: "ge" },
			{ text: "Dilma sofre impeachment", theme: "g1" },
			{ text: "Monik posa nua", theme: "gshow" },
			{ text: "Dólar supera R$10", theme: "g1" }
		];		

		return (
			<div className="panel panel-info">
				<div className="panel-heading">
					<h3 className="panel-title"><b>Mais Lidas</b></h3>
				</div>
				<div className="panel-body">					
					<div>
						{topNews.map((o, i) =>							
							<MostReadRow 
								key={i} 
								i={i+1}
								theme={o.theme} 
								text={o.text} />
						)}						
					</div>
				</div>
			</div>
		);
	}
}