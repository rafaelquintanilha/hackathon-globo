import React from 'react';
import ReactTooltip from "react-tooltip";

export default class SocialBar extends React.Component {

	constructor(props) {
		super(props);
		this.state = { color: "black" };
	}

	handleClick() {
		this.props.dismissArticle(this.props.index);
	}

	onMouseOver() {		
		this.setState({color: "red"});
	}

	onMouseOut() {
		this.setState({color: "black"});
	}
	
	render() {
		return (
			<div style={{opacity: 0.6}}>
				<i className="fa fa-facebook-official"></i>
				&nbsp;&nbsp;&nbsp;
				<i className="fa fa-twitter"></i>
				&nbsp;&nbsp;&nbsp;
				<i className="fa fa-google-plus"></i>
				<i 
					className="fa fa-thumbs-down pull-right" 
					data-tip="Remover conteúdo"
					onClick={this.handleClick.bind(this)}
					onMouseOver={this.onMouseOver.bind(this)} 
					onMouseOut={this.onMouseOut.bind(this)} 
					style={{cursor: "pointer", ...this.state}}></i>
				<ReactTooltip />
			</div>
		);
	}
}