import React from 'react';
import { BarChart } from 'react-d3-components';
import _ from "underscore";

export default class Bar extends React.Component {	
	
	render() {
		let newData = { values: [] };
		let x;
		let y;
		let chart;

		if ( !_.isEmpty(this.props.chartData) ) {
			_.each(this.props.chartData['values'], function(obj) {
				x = obj['x'].substr(0,3).toUpperCase();
				y = parseInt(obj['y']);
				newData['values'].push({x: x, y: y});
			});

			chart = (
				<BarChart
					data={newData}
					width={300}
					height={300}
					margin={{top: 10, bottom: 50, left: 50, right: 10}} />
			);
		} else chart = <p>Carregando...</p>;
		
		return (
			<div>{chart}</div>
		);
	}
}