import React from 'react';

export default class MostReadRow extends React.Component {
	
	render() {
		const mostRead = {
			fontSize: "2.25rem", 
			color: "#666"
		};		
		const { i, text, theme } = this.props; 
		return (
			<div className="row">
				<div className="col-md-2">
					<span 
						className="pull-right" 
						style={mostRead}>
						{i}
					</span>
				</div>
				<div className="col-md-offset-1 col-md-9">
					<span className={theme}>{text}</span>
					<hr />
				</div>
			</div>
		);
	}
}