import React from 'react';
import Article from '../components/Article';
//import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

export default class ArticleList extends React.Component {
	
	render() {		
		return (
			<div>			
				<ul className="media-list">				
					{this.props.articles.map((a, i) => {
						return (
							<div key={i}>
								<Article {...a} index={i} dismissArticle={this.props.dismissArticle}/>
								<hr />
							</div>		
						);					
					})}				
				</ul>
				<button type="button" className="btn btn-default btn-block">Ver Mais</button>
				<hr />
			</div>
		);
	}
}