import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../actions/actions';
//import App from '../components/App';
import Main from '../components/Main';

class AppContainer extends Component {

	static propTypes = {
		actions: PropTypes.object.isRequired,
		state: PropTypes.object.isRequired
	};

	componentWillMount() {
		this.props.actions.fetchData();
	}
	
	render() {
		console.log(this.props.state.chartHistory);
		const { isFetching, articles, chartData } = this.props.state;
		const { dismissArticle, fetchChart } = this.props.actions;
		const node = isFetching 
			? <i className="fa fa-3x fa-circle-o-notch center-fs-app" />
			: <Main articles={articles} chartData={chartData} fetchChart={fetchChart} dismissArticle={dismissArticle}/>;
		return (
			<div>{node}</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		state: state.reducer
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(actions, dispatch)
	};
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AppContainer);
