import update from 'react/lib/update';

const initialState = {
	isFetching: false,
	articles: [],
	chartData: {},
	chartHistory: []
	/*articles: [
		{ 
			tag: "Social",
			title: "BBB participa do Hackathon",
			description: "Rafael Quintanilha é o mais novo participante do reality",
			date: "2016-04-09 20:11:22",
			image: "https://scontent-mia1-1.xx.fbcdn.net/hphotos-xpt1/v/t1.0-9/12191736_10153737961829510_5607933196441375985_n.jpg?oh=f3b768aa50bf15bfa9a0c4532deec769&oe=5781BB90",
			url: "",
			theme: "g1"
		},
		{ 
			tag: "Carioca",
			title: "Vasco da Gama ganha o título",
			description: "Gigante da Colina é o mais novo campeão carioca de 2016",
			date: "2016-04-07 17:18:22",
			image: "https://upload.wikimedia.org/wikipedia/pt/8/89/Club_de_Regatas_Vasco_da_Gama.png",
			url: "",
			theme: "ge"
		},
		{ 
			tag: "Celebridades",
			title: "Ex-BBB Monik eleita a mulher mais sexy do mundo",
			description: "Vencedora do BBB 16 foi eleita pela revista Forbes",
			date: "2016-04-09 09:42:22",
			image: "http://s2.glbimg.com/jKTBMuE-FRCAkAL_Yv7LRdFDuNg=/0x0:719x620/690x0/s3.glbimg.com/v1/AUTH_e84042ef78cb4708aeebdf1c68c6cbd6/photos/apis/7483396d08c24e5ab70d84b543ccfb3e/munik-bbb16-sonha-em-apresentar-o-jornal-nacional.jpg",
			url: "",
			theme: "gshow"
		}
	]*/
};

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case "SET_FETCHING":
			return Object.assign(
				{}, 
				state,
				{ isFetching: action.isFetching }
			);

		case "DISMISS_ARTICLE":			
			return update(state, {				
				articles: { $splice: [[action.index, 1]] }
			});

		case "SET_ARTICLES":
			return Object.assign(
				{}, 
				state,
				{ articles: action.articles }
			);

		case "SET_CHART_DATA":
			return Object.assign(
				{}, 
				state,
				{ chartData: { values: action.chartData } }
			);

		case "ADD_CHART_HISTORY":
			//console.log(action.history);
			return update(state, { chartHistory: { $push: [action.history] } });				

		default:
			return state;
	}
}
