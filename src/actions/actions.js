export function setFetching(isFetching) {	
	return { 
		type: "SET_FETCHING", 
		isFetching 
	};
}

export function dismissArticle(index) {	
	return { 
		type: "DISMISS_ARTICLE", 
		index
	};
}

export function setArticles(articles) {	
	return { 
		type: "SET_ARTICLES", 
		articles
	};
}

export function setChartData(chartData) {	
	return { 
		type: "SET_CHART_DATA", 
		chartData
	};
}

export function addChartHistory(history) {	
	return { 
		type: "ADD_CHART_HISTORY", 
		history
	};
}

const ROOT = "http://52.24.9.238/meuglobo";

export function fetchData() {
	return function(dispatch, getState) {
		dispatch(setFetching(true));
		const URL = `${ROOT}/noticia/get.php`;		
		return fetch(URL)
			.then((response) => response.json())
			.then((json) => {
				dispatch(setFetching(false));
				console.log(json);
				dispatch(setArticles(json.articles));				
			})
			.catch((e) => alert(`Erro: ${e}`));
	};
}

export function fetchChart() {
	return function(dispatch, getState) {		
		const URL = `${ROOT}/dashboard/dados2.php`;
		setInterval( () => {
			return fetch(URL)
				.then((response) => response.json())
				.then((json) => {				
					console.log(json);
					//dispatch(addChartHistory(json));
					dispatch(setChartData(json));				
				})
				.catch((e) => alert(`Erro: ${e}`));
		}, 5000);		
	};
}